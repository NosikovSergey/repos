﻿using EasyApi.Models;
using EasyApi.Services.Db;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace EasyApi.Services.Auth
{
    public class AuthService
    {
        private DatabaseContext _context { get; }
        public AuthService(DatabaseContext context)
        {
            _context = context;
        }

        public string GetToken(string username, string password)
        {
            var now = DateTime.UtcNow;

            var identity = GetIdentity(username, password);
            var jwt = new JwtSecurityToken(
                issuer: AuthOptions.Issuer,
                audience: AuthOptions.Audience,
                notBefore: now,
                claims: identity.Claims,
                expires: now.Add(TimeSpan.FromMinutes(AuthOptions.LifeTime)),
                signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            return encodedJwt;
        }

        private ClaimsIdentity GetIdentity(string username, string password)
        {
            User user = _context.Users.FirstOrDefault(x => x.Login == username && x.Password == password);
            if (user == null)
                throw new AuthError();

            var claims = new List<Claim>();
            claims.Add(new Claim(ClaimsIdentity.DefaultNameClaimType, user.Login));
            claims.Add(new Claim(ClaimsIdentity.DefaultRoleClaimType, user.LevelAccess));

            return new ClaimsIdentity(
                claims,
                "Token",
                ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType
            );

        }
    }
}

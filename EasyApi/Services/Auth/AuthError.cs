﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EasyApi.Services.Auth
{
    public class AuthError : Exception
    {
        public AuthError() : base("The username or password that you have entered is invalid") { }
    }
}

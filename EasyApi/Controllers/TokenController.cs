﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EasyApi.Services.Auth;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EasyApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TokenController : ControllerBase
    {
        private AuthService _authService { get; }
        public TokenController(AuthService authService)
        {
            _authService = authService;
        }

        [HttpPost]
        public JsonResult Token([FromForm] string login, [FromForm] string pass)
        {

            try
            {
                var token = _authService.GetToken(login, pass);
                return new JsonResult(new { token, login });
            }
            catch (AuthError e)
            {
                BadRequest();
                return new JsonResult(new { error = e.Message });
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EasyApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WorkController : ControllerBase
    {
        [HttpGet]
        [Authorize(Roles = "admin")]
        public JsonResult Get()
        {
            return new JsonResult(new { value = "Этот метод доступен с уровнем доступа \"Admin\"" });
        }

        [HttpGet]
        [Authorize(Roles = "moder")]
        public JsonResult Moder()
        {
            return new JsonResult(new { value = "Этот метод доступен с уровнем доступа \"Moder\"" });
        }
    }
}